import UIKit
//Task 1
let daysArray = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
let namesArray = ["jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec"]

//выведите количество дней в каждом месяце (без имен месяцев)
print("выведите количество дней в каждом месяце (без имен месяцев)\n")

for days in daysArray {
    print("дней в месяце \(days)")
}

//используйте еще один массив с именами месяцев чтобы вывести название месяца + количество дней
print("\nиспользуйте еще один массив с именами месяцев чтобы вывести название месяца + количество дней\n")

for names in 0..<namesArray.count {
    print("\(namesArray[names]) has \(daysArray[names]) days")
}

//сделайте тоже самое, но используя массив тюплов с параметрами (имя месяца, кол-во дней)
print("\nсделайте тоже самое, но используя массив тюплов с параметрами (имя месяца, кол-во дней\n")

var daysAndNamesArray : [(names: String, days: Int)] = []

for i in 0..<namesArray.count{
    daysAndNamesArray.append((names: namesArray[i], days: daysArray[i]))
}

for i in 0..<daysAndNamesArray.count {
    print("\(daysAndNamesArray[i].names) has \(daysAndNamesArray[i].days) days")
}

//OR
print("\nOR\n")

for (names, days) in daysArray.enumerated() {
    print("\(namesArray[names]) has \(days) days")
}

//сделайте тоже самое, только выводите дни в обратном порядке (порядок в массиве не меняется)
print("\nсделайте тоже самое, только выводите дни в обратном порядке (порядок в массиве не меняется)\n")

for i in (0..<daysAndNamesArray.count).reversed() {
    print("\(daysAndNamesArray[i].names) has \(daysAndNamesArray[i].days) days")
}

//для произвольно выбранной даты (месяц и день) посчитайте количество дней до этой даты от начала года
print("\nдля произвольно выбранной даты (месяц и день) посчитайте количество дней до этой даты от начала года\n")

let birthDay = 24
var birthMonth = "mar"

var daysTo = 0

for (days, names) in namesArray.enumerated() {
    if names == birthMonth {
        for i in 0..<days  {
            daysTo += daysArray[i]
        }
        daysTo += birthDay
    }
}
print("it's \(daysTo) to my bDay from jan 1st")

//OR
print("\nOR\n")

var birthDate = (month:3, day:24)

daysTo = 0

for i in 0..<(birthDate.month - 1) {
daysTo += daysArray[i]
}
daysTo += birthDate.day

print("it's \(daysTo) to my bDay from jan 1st")
print()


let ops : [Int?] = [25, nil, 343, nil, nil, 56]

//Task 2
//Optional binding

var sum = 0

for i in ops {
    if let i = i {
        sum += i
    }
}
print("Optional binding сумма \(sum)")

//Force unwrapping

sum = 0

for i in ops{
    if i != nil{
        sum += i!
    }
}
print("\nForce unwrapping сумма \(sum)")

//Operator ??

sum = 0

for i in ops {
        sum += i ?? 0
}
print("\nOperator ?? сумма \(sum)")

//Task 3
//Алфавит наоборот
print("\nАлфавит наоборот")

let alphabet = "alphabet"

var reversedAlphabetArray = [String]()

for letter in alphabet.reversed(){
    reversedAlphabetArray.append(String(letter))
}

print()

for i in reversedAlphabetArray{
    print(i, terminator: "")
}

